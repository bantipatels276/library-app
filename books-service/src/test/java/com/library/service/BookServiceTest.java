package com.library.service;

import com.library.dto.AuthorDto;
import com.library.dto.BookDto;
import com.library.dto.GenresDto;
import com.library.entity.Author;
import com.library.entity.Book;
import com.library.entity.Genres;
import com.library.repository.AuthorRepository;
import com.library.repository.BookRepository;
import com.library.repository.GenresRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    private static final String TITLE = "title";
    private static final String FIRST_NAME = "sammer";
    private static final String LAST_NAME = "singh";
    private static final String NAME = "new version";
    private static final long ID = 1L;
    private static final int PAGE_COUNT = 100;

    @InjectMocks
    private BookService bookService;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private GenresRepository genresRepository;

    private Book book;

    @Rule
    public final ErrorCollector errorCollector = new ErrorCollector();

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        book = prepareBook();
    }

    @Test
    public void givenBooksRequestWhenGetAllBookingThenReturnListOfBooks() {
        // Arrange
        when(bookRepository.findAll()).thenReturn(Collections.singletonList(book));
        // Act
        final List<Book> bookList = bookService.getAllBooking();
        // Assert
        errorCollector.checkThat(bookList.size(), equalTo(1));
        assertForBook(bookList.get(0));
    }

    @Test
    public void givenBookIdWhenGetBookingByIdThenReturnBook() {
        // Arrange
        when(bookRepository.findById(anyLong())).thenReturn(Optional.of(book));
        // Act
        final Book bookLocal = bookService.getBookingById(ID);
        // Assert
        assertForBook(bookLocal);
    }

    @Test
    public void givenBookIdWhenGetBookingByIdThenExceptionWillBeThrown() {
        // Arrange
        when(bookRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
        // Assert
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Book Id is not exist: " + ID);
        // Act
        bookService.getBookingById(ID);
    }

    @Test
    public void givenBookIDsWhenGetBookingByIdsThenReturnListOfBooks() {
        // Arrange
        when(bookRepository.findAllById(anySet())).thenReturn(Collections.singletonList(book));
        // Act
        final List<Book> bookList = bookService.getBookingByIds(Collections.singleton(ID));
        // Assert
        errorCollector.checkThat(bookList.size(), equalTo(1));
        assertForBook(bookList.get(0));
    }

    @Test
    public void givenBookDtoWhenSaveBookThenReturnBook() {
        // Arrange
        final BookDto bookDto = new BookDto();
        bookDto.setPageCount(PAGE_COUNT);
        bookDto.setTitle(TITLE);
        final AuthorDto authorDto = new AuthorDto();
        authorDto.setFirstName(FIRST_NAME);
        authorDto.setLastName(LAST_NAME);
        bookDto.setAuthor(authorDto);
        final GenresDto genresDto = new GenresDto();
        genresDto.setName(NAME);
        bookDto.setGenres(Collections.singletonList(genresDto));
        when(bookRepository.save(any(Book.class))).thenReturn(book);
        // Art
        final Book bookLocal = bookService.saveBook(bookDto);
        // Assert
        assertForBook(bookLocal);
    }

    private Book prepareBook() {
        Book bookLocal = new Book();
        bookLocal.setId(ID);
        bookLocal.setPageCount(PAGE_COUNT);
        bookLocal.setTitle(TITLE);
        final Author author = new Author();
        author.setFirstName(FIRST_NAME);
        author.setLastName(LAST_NAME);
        bookLocal.setAuthor(author);
        final Genres genres = new Genres();
        genres.setName(NAME);
        bookLocal.setGenres(Collections.singletonList(genres));
        return bookLocal;
    }

    private void assertForBook(Book bookLocal) {
        errorCollector.checkThat(bookLocal.getId(), equalTo(ID));
        errorCollector.checkThat(bookLocal.getPageCount(), equalTo(PAGE_COUNT));
        errorCollector.checkThat(bookLocal.getTitle(), equalTo(TITLE));
        errorCollector.checkThat(bookLocal.getAuthor().getFirstName(), equalTo(FIRST_NAME));
        errorCollector.checkThat(bookLocal.getAuthor().getLastName(), equalTo(LAST_NAME));
        errorCollector.checkThat(bookLocal.getGenres().size(), equalTo(1));
        errorCollector.checkThat(bookLocal.getGenres().get(0).getName(), equalTo(NAME));
    }
}
