package com.library.service;

import com.library.dto.BookDto;
import com.library.entity.Author;
import com.library.entity.Book;
import com.library.entity.Genres;
import com.library.repository.AuthorRepository;
import com.library.repository.BookRepository;
import com.library.repository.GenresRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private GenresRepository genresRepository;

    public List<Book> getAllBooking() {
        return bookRepository.findAll();
    }

    public Book getBookingById(Long bookId) {
        Optional<Book> bookOptional = bookRepository.findById(bookId);
        if (!bookOptional.isPresent()) {
            throw new RuntimeException("Book Id is not exist: " + bookId);
        }
        return bookOptional.get();
    }

    public List<Book> getBookingByIds(Set<Long> bookIds) {
        return bookRepository.findAllById(bookIds);
    }

    public Book saveBook(BookDto bookDto) {
        Book book = new Book();
        Author author = new Author();
        BeanUtils.copyProperties(bookDto, book);
        BeanUtils.copyProperties(bookDto.getAuthor(), author);
        List<Genres> genresList = new ArrayList<>();
        bookDto.getGenres().forEach(g -> {
            Genres genres = new Genres();
            genres.setBook(book);
            BeanUtils.copyProperties(g, genres);
            genresList.add(genres);
        });
        book.setAuthor(author);
        book.setGenres(genresList);
        return bookRepository.save(book);
    }
}
