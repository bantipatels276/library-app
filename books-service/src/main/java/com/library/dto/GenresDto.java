package com.library.dto;

public class GenresDto {

    private String name;

    public GenresDto() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
