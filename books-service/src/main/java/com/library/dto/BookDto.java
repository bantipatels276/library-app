package com.library.dto;

import java.util.List;

public class BookDto {

    private String title;
    private AuthorDto author;
    private List<GenresDto> genres;
    private Integer pageCount;

    public BookDto() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AuthorDto getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDto author) {
        this.author = author;
    }

    public List<GenresDto> getGenres() {
        return genres;
    }

    public void setGenres(List<GenresDto> genres) {
        this.genres = genres;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
