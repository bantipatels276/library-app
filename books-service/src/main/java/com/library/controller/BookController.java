package com.library.controller;

import com.library.dto.BookDto;
import com.library.entity.Book;
import com.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/all")
    public List<Book> getAllBooking() {
        return bookService.getAllBooking();
    }

    @GetMapping("/{bookId}")
    public Book getBooking(@PathVariable Long bookId) {
        return bookService.getBookingById(bookId);
    }

    @PostMapping("/findByIds")
    public List<Book> getBookingByIds(@RequestBody Set<Long> bookIds) {
        return bookService.getBookingByIds(bookIds);
    }

    @PostMapping("/add")
    public Book addBook(@RequestBody BookDto bookDto) {
        return bookService.saveBook(bookDto);
    }
}
