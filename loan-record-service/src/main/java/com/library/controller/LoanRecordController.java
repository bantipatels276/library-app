package com.library.controller;

import com.library.dto.LoanRecordDto;
import com.library.entity.Book;
import com.library.entity.Borrower;
import com.library.entity.LoanRecord;
import com.library.service.LoanRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/loan-record")
public class LoanRecordController {

    @Autowired
    private LoanRecordService loanRecordService;

    @PostMapping("/add")
    public LoanRecord createLoanRecord(@RequestBody LoanRecordDto loanRecordDto) {
        return loanRecordService.createLoanRecord(loanRecordDto);
    }

    @GetMapping("/findBooksBy/{borrowerId}")
    public List<Book> findBooksById(@PathVariable Long borrowerId) {
        return loanRecordService.findBooks(borrowerId);
    }

    @GetMapping("/findBorrowerBy/{bookId}")
    public List<Borrower> findBorrowersById(@PathVariable Long bookId) {
        return loanRecordService.findBorrowers(bookId);
    }
}
