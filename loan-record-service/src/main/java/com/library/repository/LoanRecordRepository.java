package com.library.repository;

import com.library.entity.LoanRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRecordRepository extends JpaRepository<LoanRecord, Long> {
    List<LoanRecord> findByBookId(Long bookId);

    List<LoanRecord> findByBorrowerId(Long borrowerId);
}
