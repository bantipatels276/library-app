package com.library.service;

import com.library.dto.LoanRecordDto;
import com.library.entity.Book;
import com.library.entity.Borrower;
import com.library.entity.LoanRecord;
import com.library.repository.LoanRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LoanRecordService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoanRecordRepository loanRecordRepository;

    @Value("${book-service-url}")
    private String bookServiceURL;

    @Value("${borrower-service-url}")
    private String borrowerServiceURL;

    public LoanRecord createLoanRecord(LoanRecordDto loanRecordDto) {
        LoanRecord loanRecord = new LoanRecord();
        loanRecord.setBookId(loanRecordDto.getBookId());
        loanRecord.setBorrowerId(loanRecordDto.getBorrowerId());
        return loanRecordRepository.save(loanRecord);
    }

    public List<Book> findBooks(Long borrowerId) {
        List<LoanRecord> loanRecords = loanRecordRepository.findByBorrowerId(borrowerId);
        Set<Long> bookIDs = loanRecords.stream().map(record -> record.getBookId()).collect(Collectors.toSet());
        List<Book> bookList = restTemplate.postForObject(bookServiceURL + "/findByIds", bookIDs, List.class);
        return bookList;
    }

    public List<Borrower> findBorrowers(Long bookId) {
        List<LoanRecord> loanRecords = loanRecordRepository.findByBookId(bookId);
        Set<Long> borrowerIDs = loanRecords.stream().map(record -> record.getBorrowerId()).collect(Collectors.toSet());
        List<Borrower> borrowerList = restTemplate.postForObject(borrowerServiceURL + "/findByIds", borrowerIDs, List.class);
        return borrowerList;
    }
}
