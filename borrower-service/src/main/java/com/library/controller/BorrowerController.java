package com.library.controller;

import com.library.dto.BorrowerDto;
import com.library.entity.Borrower;
import com.library.service.BorrowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/borrower")
public class BorrowerController {

    @Autowired
    private BorrowerService borrowerService;

    @GetMapping("/all")
    public List<Borrower> getAllBorrower() {
        return borrowerService.getAllBorrower();
    }

    @PostMapping("/findByIds")
    public List<Borrower> getBorrowerByIds(@RequestBody Set<Long> borrowerIds) {
        return borrowerService.getBorrowersByIds(borrowerIds);
    }

    @GetMapping("/{borrowerId}")
    public Borrower getBorrowerById(@PathVariable Long borrowerId) {
        return borrowerService.getBorrower(borrowerId);
    }

    @PostMapping("/add")
    public Borrower addBorrower(@RequestBody BorrowerDto borrowerDto) {
        return borrowerService.saveBorrower(borrowerDto);
    }
}
