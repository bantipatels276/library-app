package com.library.service;

import com.library.dto.BorrowerDto;
import com.library.entity.Borrower;
import com.library.repository.BorrowerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class BorrowerService {

    @Autowired
    private BorrowerRepository borrowerRepository;

    public List<Borrower> getAllBorrower() {
        return borrowerRepository.findAll();
    }

    public Borrower getBorrower(Long borrowerId) {
        Optional<Borrower> borrowerOptional = borrowerRepository.findById(borrowerId);
        if (!borrowerOptional.isPresent()) {
            throw new RuntimeException("Borrower Id is not exist: " + borrowerId);
        }
        return borrowerOptional.get();
    }

    public List<Borrower> getBorrowersByIds(Set<Long> borrowerIds) {
        return borrowerRepository.findAllById(borrowerIds);
    }

    public Borrower saveBorrower(BorrowerDto borrowerDto) {
        Borrower borrower = new Borrower();
        BeanUtils.copyProperties(borrowerDto, borrower);
        return borrowerRepository.save(borrower);
    }
}
